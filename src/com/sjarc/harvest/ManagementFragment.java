package com.sjarc.harvest;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View.OnClickListener;
import android.view.*;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.*;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.DriveResource.MetadataResult;
import com.sjarc.harvest.data.ProduceItems.ProduceItem;
import com.sjarc.harvest.data.*;
import java.text.NumberFormat;
import java.util.Locale;


//use this to delete old data, add new produce items, etc.
public class ManagementFragment extends Fragment {

    //UI elements
    Button clearTransactions, changeHistory, changeProduce, pushData, pullData, addSubmit;
    EditText addItemName, addUnitPrice, addUnitPounds;
    Spinner addUnit;
    ListView produceList;

    ArrayAdapter<Unit> unitAdapter;
    ProduceAdapter produce;



    @Override
    public void onCreate(Bundle instance) {
        super.onCreate(instance);
        refresh();
    }

    @Override
    public View onCreateView(LayoutInflater inflator, ViewGroup container, Bundle savedInstanceState) {
        View view = inflator.inflate(R.layout.manage, container, false);

        produceList = (ListView) view.findViewById(R.id.manage_produce_list);

        clearTransactions = (Button) view.findViewById(R.id.manage_clear_transactions);
        changeProduce = (Button) view.findViewById(R.id.manage_change_cloud_produce);
        changeHistory = (Button) view.findViewById(R.id.manage_change_cloud_history);
        pushData = (Button) view.findViewById(R.id.manage_push_data);
        pullData = (Button) view.findViewById(R.id.manage_pull_data);

        addItemName = (EditText) view.findViewById(R.id.manage_add_item_name);
        addUnitPrice = (EditText) view.findViewById(R.id.manage_add_unit_price);
        addUnitPounds = (EditText) view.findViewById(R.id.manage_add_unit_pounds);
        addUnit = (Spinner) view.findViewById(R.id.manage_add_unit);
        addSubmit = (Button) view.findViewById(R.id.manage_add_submit);

        unitAdapter = new ArrayAdapter<>(Harvest.currentInstance.getApplicationContext(), R.layout.spinner, Unit.values());
        addUnit.setAdapter(unitAdapter);

        refresh();
        produceList.setAdapter(produce);
        produceList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProduceItem it = produce.getItem(position);
                addItemName.setText(it.name);
                addUnitPrice.setText(it.pricePerUnit / 100f + "");
                addUnitPounds.setText(it.poundsPerUnit + "");
                addUnit.setSelection(unitAdapter.getPosition(it.defaultUnit));
            }
        });

        addSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String name = addItemName.getText().toString();
                    int PPU = Math.round(Float.parseFloat(addUnitPrice.getText().toString()) * 100);
                    Unit u = (Unit) addUnit.getSelectedItem();
                    float lbPU = Float.parseFloat(addUnitPounds.getText().toString());
                    if (name == null || name.length() < 1) throw new NumberFormatException("Name too short!");
                    if (PPU < 1) throw new NumberFormatException("Price per Unit too low!");
                    if (lbPU <= 0) throw new NumberFormatException("Pounds per Unit too low!");


                    ProduceItems.addProduce(name, u, PPU, lbPU, true);
                    IOManager.pushToCloud(true, false); // push produce to cloud
                    refresh();

                    addItemName.setText(null);
                    addUnitPrice.setText(null);
                    addUnitPounds.setText(null);
                    //decided to not reset spinner

                } catch (NumberFormatException ex) {
                    Harvest.showError("Could not parse information into a Produce Item", ex);
                }

            }
        });

        clearTransactions.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //clear transactions
                AlertDialog dialog = new AlertDialog.Builder(Harvest.currentInstance)
                        .setPositiveButton("Yes", new android.content.DialogInterface.OnClickListener() {
                                       @Override
                                       public void onClick(DialogInterface dialog, int which) {
                                           //affirmative code
                                           IOManager.clearTransactionHistory();
                                           IOManager.pushToCloud(false, true);
                                       }
                                   })
                        .setNegativeButton("No", new android.content.DialogInterface.OnClickListener() {
                                       @Override
                                       public void onClick(DialogInterface dialog, int which) {
                                           //negative code, order canceled
                                       }
                                   }).create();
                dialog.setTitle("Are you sure?");
                dialog.setMessage("Do you want to remove the transaction history? It cannot be recovered!");
                dialog.show();
            }
        });

        changeProduce.setText(Harvest.currentInstance.getString(R.string.manage_change_cloud_produce_title) + " (Currently " + "NOT SET!" + ")");
        changeProduce.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //clear transactions
                IOManager.changeCloudProduceFile();
            }
        });

        changeHistory.setText(Harvest.currentInstance.getString(R.string.manage_change_cloud_history_title) + " (Currently " + "NOT SET!" + ")");
        changeHistory.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //clear transactions
                IOManager.changeCloudHistoryFile();
            }
        });

        pushData.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //clear transactions
                IOManager.pushToCloud(true, true);
            }
        });

        pullData.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //clear transactions
                IOManager.pullFromCloud(true, true, new Triggerable() {
                                    @Override
                                    public void trigger() {
                                        refresh();
                                    }
                                });

            }
        });

        return view;
    }

    public void refresh() {
        //update button titles, refresh list

        Harvest.currentInstance.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (IOManager.cloudTransactionHistory != null && changeHistory != null) {
                    IOManager.cloudTransactionHistory.getMetadata(Harvest.currentInstance.apiClient).setResultCallback(new ResultCallback<MetadataResult>() {
                        @Override
                        public void onResult(MetadataResult res) {
                            changeHistory.setText(Harvest.currentInstance.getString(R.string.manage_change_cloud_history_title) + " (Currently '" + res.getMetadata().getTitle() + "')");
                        }
                    });
                }
                if (IOManager.cloudProduceItems != null && changeProduce != null) {
                    IOManager.cloudProduceItems.getMetadata(Harvest.currentInstance.apiClient).setResultCallback(new ResultCallback<MetadataResult>() {
                        @Override
                        public void onResult(MetadataResult res) {
                            changeProduce.setText(Harvest.currentInstance.getString(R.string.manage_change_cloud_produce_title) + " (Currently '" + res.getMetadata().getTitle() + "')");
                        }
                    });
                }

                //produce list refresh
                if (produce == null) produce = new ProduceAdapter(Harvest.currentInstance.getApplicationContext());
                produce.clear();
                produce.addAll(ProduceItems.getProduce());
                produce.notifyDataSetChanged();
            }
        });
    }


    class ProduceAdapter extends ArrayAdapter<ProduceItem> {

        private final Context context;
        LayoutInflater inflater;

        public ProduceAdapter(Context context) {
            super(context, R.layout.manage_row);
            this.context = context;
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            convertView = inflater.inflate(R.layout.manage_row, parent, false);

            final ProduceItem item = getItem(position);

            //init row view
            TextView itemName, unit, unitPrice, unitPounds;
            itemName = (TextView) convertView.findViewById(R.id.manage_row_item_name);
            unit = (TextView) convertView.findViewById(R.id.manage_row_unit);
            unitPrice = (TextView) convertView.findViewById(R.id.manage_row_unit_price);
            unitPounds = (TextView) convertView.findViewById(R.id.manage_row_unit_pounds);

            itemName.setText(item.name);
            unit.setText(item.defaultUnit.name());
            unitPrice.setText(NumberFormat.getCurrencyInstance(Locale.US).format(item.getPriceOf(1) / 100f));

            if (Math.abs(item.poundsPerUnit - Math.round(item.poundsPerUnit)) < 0.01f) unitPounds.setText(Math.round(item.poundsPerUnit) + " lbs");
            else unitPounds.setText(item.poundsPerUnit + " lbs");


            return convertView;
        }

        @Override
        public String toString() {
            StringBuilder b = new StringBuilder();

            for (int i = 0; i < getCount(); i++) {
                b.append(getItem(i)).append("  :-:  ");
            }

            return b.toString();
        }
    }
}
