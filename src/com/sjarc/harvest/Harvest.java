package com.sjarc.harvest;

import android.app.*;
import android.content.DialogInterface.OnClickListener;
import android.content.*;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.widget.*;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.drive.*;
import com.sjarc.harvest.data.IOManager;
import com.sjarc.harvest.data.ProduceItems;
import java.util.ArrayList;

public class Harvest extends Activity implements ConnectionCallbacks, OnConnectionFailedListener {

    public static final boolean PUSH_ON_SAVE = false;
    public static final int RESULT_CLOUD_HISTORY = RESULT_FIRST_USER + 1;
    public static final int RESULT_CLOUD_PRODUCE = RESULT_FIRST_USER + 2;
    public static final int RESULT_CONNECTION = RESULT_FIRST_USER + 4;

    public static Harvest currentInstance;

    public static ArrayList<AlertDialog> Errors = new ArrayList<>();

    //use a short-length toast if toast length is less than this value

    public static final int TOAST_LENGTH_TO_SHORT_IF_LESS = 24;
    public SharedPreferences settings;

    ActionBar.Tab histTab, addTab, produceTab;
    public TransactionFragment transactionFragment = new TransactionFragment();
    public OrderFragment orderFragment = new OrderFragment();
    public ManagementFragment managementFragment = new ManagementFragment();

    public GoogleApiClient apiClient;
    public boolean connected = false;


    public Harvest() {
        currentInstance = this;
    }

    @Override
    public void onConnected(Bundle bundle) {
//        currentInstance.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                // do something to show error
//                //Log.e("Harvest", errorMsg + " : " + t, t);
//                //Toast.makeText(currentInstance.context, errorMsg, Toast.LENGTH_LONG).show();
//
//                AlertDialog msg = new AlertDialog.Builder(currentInstance).create();
//                msg.setTitle("Login");
//                msg.setMessage("Connected!");
//                msg.setCanceledOnTouchOutside(true);
//                msg.show();
//            }
//        });
        //have to include empty array or reflection is weird at startup for some reason
        IOManager.initCloud(new Triggerable[]{});
        connected = true;
    }

    @Override
    public void onConnectionFailed(final ConnectionResult connectionResult) {
//        currentInstance.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                // do something to show error
//                //Log.e("Harvest", errorMsg + " : " + t, t);
//                //Toast.makeText(currentInstance.context, errorMsg, Toast.LENGTH_LONG).show();
//
//                AlertDialog msg = new AlertDialog.Builder(currentInstance).create();
//                msg.setTitle("ERROR");
//                msg.setMessage("Connection failed - " + connectionResult.getErrorMessage());
//                msg.setCanceledOnTouchOutside(true);
//                msg.show();
//            }
//        });
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, RESULT_CONNECTION);
            } catch (IntentSender.SendIntentException e) {
                // Unable to resolve, message user appropriately
                //apiClient.connect();
                showError("Unable to reconnect", e);
            }
        } else {
            GoogleApiAvailability.getInstance().getErrorDialog(this, connectionResult.getErrorCode(), 0).show();
        }
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {


        //Toast.makeText(this, "Activity Result: " + resultCode + " | " + requestCode, Toast.LENGTH_LONG).show();

        switch (requestCode) {
            case RESULT_CONNECTION:
                switch (resultCode) {
                    case RESULT_OK:
                        if (!apiClient.isConnecting() && !apiClient.isConnected()) apiClient.connect();
                        break;
                    case RESULT_CANCELED:
                        showError("Connection canceled, reconnecting!");
                        apiClient.connect();
                        break;
                    default:
                        showError("Connection failed, unable to reconnect - Result " + resultCode);
                        break;
                }
                break;
            case RESULT_CLOUD_PRODUCE:
                if (resultCode == RESULT_OK) {
                    IOManager.setCloudProduceFile((DriveId) data.getParcelableExtra(OpenFileActivityBuilder.EXTRA_RESPONSE_DRIVE_ID));
                } else if (resultCode != RESULT_CANCELED) {
                    showError("Error in setting produce item file, got result code: " + resultCode);
                }
                break;
            case RESULT_CLOUD_HISTORY:
                if (resultCode == RESULT_OK) {
                    IOManager.setCloudHistoryFile((DriveId) data.getParcelableExtra(OpenFileActivityBuilder.EXTRA_RESPONSE_DRIVE_ID));
                } else if (resultCode != RESULT_CANCELED) {
                    showError("Error in setting transaction history file, got result code: " + resultCode);
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        showError("Connection suspended - " + i);
    }

    @Override
    public void onStart() {
        super.onStart();
        apiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        apiClient.disconnect();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        IOManager.initFiles();
        //IOManager.checkAndDoFirstInit();
        ProduceItems.init();
        //should init when needed - in transaction tab
        //History.init();

        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        settings = getSharedPreferences("harvest_settings", 0);

        apiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Drive.API)
                .addScope(Drive.SCOPE_FILE)
                .build();



        setContentView(R.layout.main);

        ActionBar actionBar = getActionBar();

        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        histTab = actionBar.newTab().setText(R.string.list_tab_title);
        addTab = actionBar.newTab().setText(R.string.add_tab_title);
        produceTab = actionBar.newTab().setText(R.string.management_tab_title);

        histTab.setTabListener(new TabListener(transactionFragment));
        addTab.setTabListener(new TabListener(orderFragment));
        produceTab.setTabListener(new TabListener(managementFragment));

        actionBar.addTab(histTab);
        actionBar.addTab(addTab);
        actionBar.addTab(produceTab);

        //ensure first tab always selected first
        actionBar.setSelectedNavigationItem(0);
    }

    public void addRow(final String notes, final String... elements) {
        currentInstance.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TransactionFragment) transactionFragment).addRow(notes, elements);
            }
        });
    }

////////////////////////////
//  Non-Activity Methods  //
////////////////////////////

    public static final String INFO_REQUEST_NO_INFO = "#NO_RESULT#";
    private static String requestInfo_result;

    /**
     * MUST CALL THIS FROM A BACKGROUND THREAD!!!
     *
     * @param title
     * @param description
     * @param hint
     * @return
     */
    public synchronized static String requestInfo(final String title, final String description, final String hint) {

        final Object monitor = new Object();
        requestInfo_result = INFO_REQUEST_NO_INFO;

        currentInstance.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i("Harvest", "Requesting info with title " + title + ", description " + description + ", and hint " + hint);

                AlertDialog.Builder builder = new AlertDialog.Builder(currentInstance);
                builder.setTitle(title);

                final LinearLayout layout = new LinearLayout(currentInstance);

                if (description != null && description.length() > 0) {
                    TextView t = new TextView(currentInstance);
                    t.setText(description);
                    layout.addView(t);
                }

                final EditText input = new EditText(currentInstance);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                input.setHint(hint);
                layout.addView(input);

                builder.setView(layout);

                builder.setCancelable(false);
                builder.setPositiveButton("OK", new OnClickListener() {
                                      @Override
                                      public void onClick(DialogInterface dialog, int which) {
                                          requestInfo_result = input.getText().toString();
                                          monitor.notify();
                                      }
                                  });
                builder.show();
            }
        });

        try {
            monitor.wait();
        } catch (InterruptedException e) {
        }

        Log.i("Harvest", "Got info from request titled " + title + ": " + requestInfo_result);
        return requestInfo_result;
    }

    public static void showError(final String errorMsg, final Throwable t) {
        currentInstance.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // do something to show error
                Log.e("Harvest", errorMsg, t);
                //Toast.makeText(currentInstance.context, errorMsg, Toast.LENGTH_LONG).show();

                AlertDialog msg = new AlertDialog.Builder(currentInstance).create();
                msg.setTitle("ERROR");
                msg.setMessage(errorMsg + " - " + t);
                msg.setCanceledOnTouchOutside(true);
                msg.show();
            }
        });
    }

    public static void showError(final String errorMsg) {
        currentInstance.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // do something to show error
                Log.e("Harvest", errorMsg);
                //Toast.makeText(currentInstance.context, errorMsg, Toast.LENGTH_LONG).show();

                AlertDialog msg = new AlertDialog.Builder(currentInstance).create();
                msg.setTitle("ERROR");
                msg.setMessage(errorMsg);
                msg.setCanceledOnTouchOutside(true);
                msg.show();
            }
        });
    }

    public static void log(final Object toLog) {
        currentInstance.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i("Harvest-log", toLog.toString());
                Toast.makeText(currentInstance, toLog.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
