package com.sjarc.harvest;

public interface Triggerable {
    public void trigger();
}
