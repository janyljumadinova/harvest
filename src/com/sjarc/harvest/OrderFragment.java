package com.sjarc.harvest;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View.OnClickListener;
import android.view.*;
import android.view.inputmethod.CompletionInfo;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.*;
import com.sjarc.harvest.data.History;
import com.sjarc.harvest.data.History.Transaction;
import com.sjarc.harvest.data.ProduceItems;
import com.sjarc.harvest.data.ProduceItems.ProduceItem;
import java.text.NumberFormat;
import java.util.*;

public class OrderFragment extends ListFragment {

    public static ArrayAdapter<String> autoProduce;
    AddAdapter transElements;

    public OrderFragment() {

    }

    @Override
    public void onCreate(Bundle instance) {
        super.onCreate(instance);
        refreshAutoProduce();
        transElements = new AddAdapter(getActivity().getApplicationContext());
        transElements.add(new TransactionElement());
        setListAdapter(transElements);
    }

    @Override

    public View onCreateView(LayoutInflater inflator, ViewGroup container, Bundle savedInstanceState) {
        View v = inflator.inflate(R.layout.add, container, false);

        Button submit = (Button) v.findViewById(R.id.add_submit);

        submit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View c) {
                //add all specified TransactionElements to the History
                Date d = new Date();
                int orderId = History.getNewestOrderId();
                final Transaction[] trans = new Transaction[transElements.getCount()];

                for (int i = 0; i < trans.length; i++) {
                    TransactionElement el = transElements.getItem(i);

                    if (el.itemName == null || "".equals(el.itemName)) {
                        Harvest.log("Produce name must be specified!");
                        return;
                    } else if (el.quantity == 0) {
                        Harvest.log("Quantity cannot be 0!");
                        return;
                    }

                    ProduceItem p;
                    try {
                        p = ProduceItems.getProduce(el.itemName);
                    } catch (IllegalArgumentException ex) {
                        //this should not trigger unless there are no possible valid produce in the selection.
                        Harvest.showError(el.itemName + " is not a valid Produce Item! Please use the Manage tab to create any new produce.");
                        return;
                    }
                    //hit an error, let it display from previous call but do not add transactions or navigate away
                    if (p == null) return;
                    trans[i] = new Transaction(p, el.quantity, el.seller, el.notes, el.paymentMethod, d, ++orderId);
                }

                AlertDialog dialog = new AlertDialog.Builder(Harvest.currentInstance)
                        .setPositiveButton("Paid", new android.content.DialogInterface.OnClickListener() {
                                       @Override
                                       public void onClick(DialogInterface dialog, int which) {
                                           //affirmative code

                                           //add transactions
                                           for (Transaction t : trans) {
                                               History.addTransaction(t, true);
                                           }
                                           //switch to list view, which is always at index 0
                                           getActivity().getActionBar().setSelectedNavigationItem(0);
                                       }
                                   })
                        .setNegativeButton("Cancel", new android.content.DialogInterface.OnClickListener() {
                                       @Override
                                       public void onClick(DialogInterface dialog, int which) {
                                           //negative code, order canceled
                                       }
                                   }).create();

                dialog.setCanceledOnTouchOutside(true);
                dialog.setTitle("Order");

                StringBuilder b = new StringBuilder();
                String[] descs = new String[trans.length];
                int longe = 12;
                float total = 0;
                for (int i = 0; i < trans.length; i++) {
                    descs[i] = trans[i].quantity() + "  " + trans[i].name();
                    if (descs[i].length() > longe) longe = descs[i].length();
                    total += trans[i].total();
                }

                //buffer
                longe += 2;
                for (int i = 0; i < descs.length; i++) {
                    b.append(descs[i]);
                    for (int j = 0; j < longe - descs[i].length(); j++) {
                        b.append(' ');
                    }
                    b.append(NumberFormat.getCurrencyInstance(Locale.US).format(trans[i].total())).append('\n');
                }

                b.append('\n').append("Order Total:");
                for (int j = 0; j < longe - 12; j++) {
                    b.append(' ');
                }
                b.append(NumberFormat.getCurrencyInstance(Locale.US).format(total));


                dialog.setMessage(b.toString());
                dialog.show();
                TextView tv = (TextView) dialog.findViewById(android.R.id.message);
                tv.setTypeface(Typeface.MONOSPACE);
                tv.setGravity(Gravity.CENTER);
                tv.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                tv.setTextSize(14);

            }
        });


        Button newRow = (Button) v.findViewById(R.id.add_new_row);

        newRow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View c) {
                //add new row
                transElements.add(new TransactionElement());
            }
        });

        return v;
    }

    public void refreshAutoProduce() {
        //must re-navigate tabs or refresh page to recreate views and adapter
        //also ensure we have an activity first
        if (getActivity() != null) autoProduce = new ArrayAdapter<>(getActivity().getApplicationContext(), R.layout.spinner, ProduceItems.produceNames());
    }


    static class AddAdapter extends ArrayAdapter<TransactionElement> {

        private final Context context;
        LayoutInflater inflater;

        public AddAdapter(Context context) {
            super(context, R.layout.add_row, new ArrayList<TransactionElement>());
            this.context = context;
            this.inflater = LayoutInflater.from(context);

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            convertView = (LinearLayout) inflater.inflate(R.layout.add_row, parent, false);

            final TransactionElement element = (TransactionElement) getItem(position);

            final AutoCompleteTextView itemName = (AutoCompleteTextView) convertView.findViewById(R.id.add_itemName);
            final EditText quantity = (EditText) convertView.findViewById(R.id.add_quantity);
            final Spinner seller = (Spinner) convertView.findViewById(R.id.add_seller);
            final Spinner paymentMethod = (Spinner) convertView.findViewById(R.id.add_payment_method);
            final EditText notes = (EditText) convertView.findViewById(R.id.add_notes);
            final TextView total = (TextView) convertView.findViewById(R.id.add_total);

            //ensure values are carried over, could result in weird issues with skipping back on input, so it's a hack
            itemName.setText(element.itemName);
            if (element.quantity != 0) quantity.setText("" + element.quantity);
            seller.setSelection(element.sellerPos);
            paymentMethod.setSelection(element.paymentPos);
            notes.setText(element.notes);
            total.setText(NumberFormat.getCurrencyInstance(Locale.US).format(element.total()));

            //ensure TransactionElement is updated by updating when focus changes

            itemName.setAdapter(autoProduce);
            itemName.setThreshold(1);
            itemName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus && itemName.enoughToFilter()) itemName.showDropDown();
                }
            });

            itemName.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {
                    element.itemName = itemName.getText().toString();
                    total.setText(NumberFormat.getCurrencyInstance(Locale.US).format(element.total()));
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    //ignore
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    //ignore
                }
            });


            //crashes if no possible auto-completion
            //should also execute this if/when add button is pressed
            itemName.setOnEditorActionListener(new TextView.OnEditorActionListener() {

                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (itemName.isPopupShowing() || itemName.enoughToFilter()) {
                        itemName.showDropDown();
                        itemName.onCommitCompletion(new CompletionInfo(0, 0, null));
                        //this may not be needed here
                        total.setText(NumberFormat.getCurrencyInstance(Locale.US).format(element.total()));
                    }
                    return true;
                }
            });


            quantity.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        element.quantity = Float.parseFloat(quantity.getText().toString());
                        total.setText(NumberFormat.getCurrencyInstance(Locale.US).format(element.total()));
                    } catch (Exception ex) {
                        //can't parse number yet, ignore
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    //ignore
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    //ignore
                }
            });

            seller.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    element.seller = seller.getSelectedItem().toString();
                    element.sellerPos = position;

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    //ignore, but do default here
                    element.seller = Harvest.currentInstance.getResources().getStringArray(R.array.add_seller_entries)[0];
                    element.sellerPos = 0;
                }

            });

            paymentMethod.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    element.paymentMethod = paymentMethod.getSelectedItem().toString();
                    element.paymentPos = position;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    //ignore, but do default here
                    element.paymentMethod = Harvest.currentInstance.getResources().getStringArray(R.array.add_payment_method_entries)[0];
                    element.paymentPos = 0;
                }

            });

            notes.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                    element.notes = notes.getText().toString();
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    //ignore
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    //ignore
                }
            });

            return convertView;
        }
    }

    static class TransactionElement {
        String itemName;
        float quantity;
        String seller;
        int sellerPos;
        String paymentMethod;
        int paymentPos;
        String notes;

        public float total() {
            if (ProduceItems.isProduce(itemName) && quantity > 0) {
                return ProduceItems.getProduce(itemName).getPriceOf(quantity) / 100f;
            } else return 0f;
        }
    }
}
