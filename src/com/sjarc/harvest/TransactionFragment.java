package com.sjarc.harvest;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import com.sjarc.harvest.data.History;
import com.sjarc.harvest.data.IOManager;

public class TransactionFragment extends ListFragment {


    private RowViewAdapter table;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Harvest.transactionList = this;
        table = new RowViewAdapter(getActivity().getApplicationContext());
        setListAdapter(table);

        //before we init (or re-init) we must ensure all changes are dumped to disk
        IOManager.saveNewTransactions();
        //now we init
        History.init();

        //things appearing in file but not in app

        //History.addTransaction(new History.Transaction(ProduceItems.getProduce("Beets"), 1f, "Market", "Gator Cash"));
        //History.addTransaction(new History.Transaction(ProduceItems.getProduce("Basil"), 1.5f, "Market", "Credit Card"));
        //History.addTransaction(new History.Transaction(ProduceItems.getProduce("Carrots"), 1.35f, "Market", "Cash"));

    }

    @Override
    public View onCreateView(LayoutInflater inflator, ViewGroup container, Bundle savedInstanceState) {
        return inflator.inflate(R.layout.list, container, false);
    }

    public void clear() {
        Harvest.currentInstance.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (table != null) {
                    table.clear();
                    table.notifyDataSetChanged();
                }
            }
        });
    }

    public void addRow(String notes, String... elements) {
        if (table == null) Harvest.showError("History table is null, restart the app!");
        if (elements.length != Row.COLS) {
            //serious problem here... we appear to hit this

            Harvest.showError("Row added with incorrect number of entries - whaaa?");

        }

        table.insert(new Row(notes, elements), 0);

        IOManager.saveNewTransactions();
        //remove if only supposed to do on button in manage
        if (Harvest.PUSH_ON_SAVE) IOManager.pushToCloud(false, true);
    }

    @Override
    public void onListItemClick(ListView list, View view, int position, long id) {
        super.onListItemClick(list, view, position, id);

        //debug stuff
        //History.addTransaction(new History.Transaction(ProduceItems.getProduce(ProduceItems.PRODUCE_NAMES[(int) (Math.random() * 23)]), ((int) (Math.random() * 1000f)) / 100f, "Market", "Cash"));

        Row r = (Row) list.getItemAtPosition(position);
        Toast.makeText(Harvest.currentInstance, r.notes(), r.notes().length() < Harvest.TOAST_LENGTH_TO_SHORT_IF_LESS ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG
        ).show();
    }

    public class RowViewAdapter extends ArrayAdapter<Row> {

        private final Context context;
        LayoutInflater inflater;

        public RowViewAdapter(Context context) {
            super(context, R.layout.list_row);
            this.context = context;
            this.inflater = LayoutInflater.from(context);

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            convertView = inflater.inflate(R.layout.list_row, parent, false);

            Row item = getItem(position);
            if (item == null) return convertView;
            if (item.cols.length != Row.COLS) return convertView;

            try {
                TextView col1 = (TextView) convertView.findViewById(R.id.col1);
                col1.setText(item.getCol(1));

                TextView col2 = (TextView) convertView.findViewById(R.id.col2);
                col2.setText(item.getCol(2));

                TextView col3 = (TextView) convertView.findViewById(R.id.col3);
                col3.setText(item.getCol(3));

                TextView col4 = (TextView) convertView.findViewById(R.id.col4);
                col4.setText(item.getCol(4));

                TextView col5 = (TextView) convertView.findViewById(R.id.col5);
                col5.setText(item.getCol(5));

                TextView col6 = (TextView) convertView.findViewById(R.id.col6);
                col6.setText(item.getCol(6));

                TextView col7 = (TextView) convertView.findViewById(R.id.col7);
                col7.setText(item.getCol(7));

                TextView col8 = (TextView) convertView.findViewById(R.id.col8);
                col8.setText(item.getCol(8));

                TextView col9 = (TextView) convertView.findViewById(R.id.col9);
                col9.setText(item.getCol(9));

                TextView col10 = (TextView) convertView.findViewById(R.id.col10);
                col10.setText(item.getCol(10));
            } catch (NullPointerException ex) {
                //FOR SOME FUCKING REASON I GET A NULL POINTER HERE BUT ONLY SOMETIMES. ANDROID IS FUCKING WITH ME.
                //the col vars return as null, unable to find view by id I guess

                Harvest.showError("Instantiation Null Pointer Error!", ex);

            }

            return convertView;
        }



    }

    public static class Row {

        public static final int COLS = 10;

        private final String[] cols;
        private final String notes;

        public Row(String notes, String... cols) {
            //if(cols.length != COLS) throw new Error("Rows should always have " + COLS + " columns!");
            this.cols = cols;
            this.notes = notes;
        }

        public String getCol(int indexFrom1) {
            return cols[indexFrom1 - 1];
        }

        public String notes() {
            return notes;
        }

    }
}
