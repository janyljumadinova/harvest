package com.sjarc.harvest.data;

import com.sjarc.harvest.Harvest;
import java.util.*;

public final class ProduceItems {

    private static final HashMap<String, ProduceItem> PRODUCE = new HashMap<>();


    //SAVE THESE IN A FILE SOMEWHERE, FOR MODIFICATION AND SO I DON'T HAVE TO TYPE

    public static final void init() {
        //load from save
        //ensure produce is empty first!
        PRODUCE.clear();
        IOManager.loadProduceItems();
    }

    public static final ProduceItem addProduce(String name, Unit defaultUnit, int pricePerUnit, float poundsPerUnit, boolean doSave) {
        //check if produce is already extant
        //if (PRODUCE.containsKey(name.toLowerCase())) return null;
        ProduceItem p = new ProduceItem(name, pricePerUnit, defaultUnit, poundsPerUnit);

        PRODUCE.put(name.toLowerCase(), p);

        //ADD NEW TO AUTO-COMPLETION
        //Harvest.autoProduce.add(name);


        if (doSave) IOManager.saveProduceItems();

        return p;
    }

    public static final ProduceItem addProduce(String saved, boolean doSave) {
        String name;
        int pricePerUnit = 100;
        Unit defaultUnit = Unit.ITEMS;
        float poundsPerUnit = 1f;

        //load data from string
        saved = saved.trim();
        String[] dat = saved.split("" + ProduceItem.DELIM);
        name = dat[0].trim();
        try {
            defaultUnit = Unit.valueOf(dat[1].trim().toUpperCase());
            pricePerUnit = Integer.parseInt(dat[2].trim());
            poundsPerUnit = Float.parseFloat(dat[3].trim());
        } catch (Exception ex) {
            Harvest.showError("Could not parse " + saved + " into ProduceItem", ex);
        }
        return addProduce(name, defaultUnit, pricePerUnit, poundsPerUnit, doSave);
    }

    public static final ProduceItem getProduce(String name) throws IllegalArgumentException {
        ProduceItem r = PRODUCE.get(name.toLowerCase());
        if (r == null) {
            throw new IllegalArgumentException("No produce named " + name);
        }
        return r;
    }

    public static final List<ProduceItem> getProduce() {
        ArrayList<ProduceItem> r = new ArrayList<>(PRODUCE.values());
        Collections.sort(r);
        return r;
    }

    public static final List<String> produceNames() {

        List<String> r = new ArrayList<>(PRODUCE.size());

        for (ProduceItem p : PRODUCE.values()) {
            r.add(p.name);
        }

        Collections.sort(r);

        return r;
        //this does not use correct capitalization since keys are all lowercase
        //return new ArrayList<>(PRODUCE.keySet());
    }

    public static final boolean isProduce(String name) {
        if (name == null || "".equals(name)) return false;
        return PRODUCE.containsKey(name.toLowerCase());
    }

    /**
     * A single Item of produce. Only one instance of this class should exist, and be passed around.
     */
    public static final class ProduceItem implements Comparable<ProduceItem> {

        public static final char DELIM = ':';

        /**
         * The name of the item.
         */
        public final String name;

        /**
         * The price of the item in cents.
         */
        public final int pricePerUnit;

        /**
         * The default unit type to use with this ProduceItem.
         */
        public final Unit defaultUnit;

        /**
         * The number of pounds in one defaultUnit of this ProduceItem.
         */
        public final float poundsPerUnit;


        private ProduceItem(String nm, int price, Unit dUnit, float pounds) {
            name = nm;
            pricePerUnit = price;
            defaultUnit = dUnit;
            poundsPerUnit = pounds;
        }

        @Override
        public int compareTo(ProduceItem another) {
            return name.compareTo(another.name);
        }

        //this still returns in cents
        public final int getPriceOf(float quantity) {
            return (int) (quantity * pricePerUnit); // round down if for some reason isn't an int already
        }

        public final float getWeightOf(float quantity) {
            //limit to two decimal places
            return (Math.round(quantity * poundsPerUnit * 100f)) / 100f;
        }

        public final String getSaveString() {
            return name + DELIM + defaultUnit + DELIM + pricePerUnit + DELIM + poundsPerUnit;
        }

        @Override
        public String toString() {
            return "[ " + name + " | " + defaultUnit + " | " + pricePerUnit + " | " + poundsPerUnit + " ]";
        }

    }

}
