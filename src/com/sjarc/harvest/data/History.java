package com.sjarc.harvest.data;

import com.sjarc.harvest.Harvest;
import com.sjarc.harvest.data.ProduceItems.ProduceItem;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public final class History {

    private final static TreeMap<Long, List<Transaction>> HISTORY = new TreeMap<>();

    public static final void init() {
        //load from save here
        //ensure rows are empty first!
        Harvest.currentInstance.transactionFragment.clear();
        HISTORY.clear();
        IOManager.loadPastTransactions();


    }

    public static final void addTransaction(Transaction t, boolean newTransaction) {
        List<Transaction> l = HISTORY.get(t.timeOf.getTime());
        if (l == null) {
            ArrayList<Transaction> list = new ArrayList<>();
            list.add(t);
            HISTORY.put(t.timeOf.getTime(), list);
        } else {
            l.add(t);
        }

        //add new row
        Harvest.currentInstance.addRow(
                t.notes,
                t.getFormattedOrderId(),
                t.time(),
                t.soldBy,
                t.paymentMethod,
                t.itemSold.name,
                t.quantitySold % 1 == 0 ? "" + ((int) t.quantitySold) : "" + t.quantitySold,
                t.unit().getUnitIdentifierSH(t.quantitySold),
                t.getPounds() % 1 == 0 ? "" + ((int) t.getPounds()) : "" + t.getPounds(),
                NumberFormat.getCurrencyInstance(Locale.US).format(t.pricePerUnit()),
                NumberFormat.getCurrencyInstance(Locale.US).format(t.total())
        );

        if (newTransaction) IOManager.registerNewTransaction(t);
    }

//    public static String longToOrderID(long l) {
//        return Long.toString(l - 1450000000000L, 36).toUpperCase();
//    }
//
//    public static long orderIDToLong(String id) {
//        return Long.parseLong(id, 36) + 1450000000000L;
//    }

    public static int getNewestOrderId() {
        int id;
        try {
            id = 0;
            for (Transaction t : HISTORY.lastEntry().getValue()) {
                if (t.getOrderId() > id) id = t.getOrderId();
            }
        } catch (Exception ex) {
            //use default in case of history error
            id = 0;
            Harvest.log("Using Default Order ID 1 - Could not find History! (This can be manually corrected)");
        }
        return id;
    }

    public static final Collection<Transaction> getTransactions() {
        List<Transaction> l = new ArrayList<>();

        for (List<Transaction> a : HISTORY.values()) {
            l.addAll(a);
        }
        //sort transactions
        Collections.sort(l);
        return l;
    }

    public static final class Transaction implements Comparable<Transaction> {

        public static final char DELIM = ';';
        static int nextInternalID = 0;

        int internalID = nextInternalID++;

        Date timeOf;

        int orderId;

        // col 1
        String soldBy;

        // col 2
        ProduceItem itemSold;

        // col 3
        float quantitySold;

        String paymentMethod;

        String notes;

        public Transaction(ProduceItem itemSold, float quantitySold, String soldBy, String notes, String paymentMethod, Date timeOf, int orderId) {
            this.soldBy = soldBy;
            this.itemSold = itemSold;
            this.quantitySold = quantitySold;
            this.notes = (notes == null || notes.length() == 0) ? "No note" : notes;
            this.paymentMethod = paymentMethod;
            this.timeOf = timeOf;
            this.orderId = orderId % 1_000_000_000; // wrap to max of 1 billion
        }

        public Transaction(ProduceItem itemSold, float quantitySold, String soldBy, String notes, String paymentMethod, int orderId) {
            //is created with current date/time
            this(itemSold, quantitySold, soldBy, notes, paymentMethod, new Date(), orderId);
        }

        public Transaction(ProduceItem itemSold, float quantitySold, String soldBy, String paymentMethod, int orderId) {
            this(itemSold, quantitySold, soldBy, "No note", paymentMethod, orderId);
        }

        public Transaction(String saved) {
            //format: timeOf.getTime() + DELIM + soldBy + DELIM + itemSold.name + DELIM + quantitySold
            saved = saved.trim();
            String[] dat = saved.split(Character.toString(DELIM));

            try {
                orderId = Integer.parseInt(dat[0].trim());
                timeOf = new Date(Long.parseLong(dat[1].trim()));
                soldBy = dat[2].trim();
                paymentMethod = dat[3].trim();
                itemSold = ProduceItems.getProduce(dat[4].trim());
                quantitySold = Float.parseFloat(dat[5].trim());
                if (dat.length > 4 && dat[6].trim().length() != 0) notes = dat[6].trim(); // notes could be empty string
                else notes = "No note";
            } catch (Exception ex) {
                Harvest.showError("Unable to create Transaction from " + saved, ex);
            }


        }

        @Override
        public int compareTo(Transaction another) {
            int i = another.timeOf.compareTo(timeOf);
            if (i != 0) return i;
            else return another.internalID - internalID;
        }

        public String name() {
            return itemSold.name;
        }

        public float quantity() {
            return quantitySold;
        }

        // col 6
        public float total() {
            return itemSold.getPriceOf(quantitySold) / 100f;
        }

        // col 5
        public float pricePerUnit() {
            return itemSold.pricePerUnit / 100f;
        }

        // col 4
        public Unit unit() {
            return itemSold.defaultUnit;
        }

        public String note() {
            return notes;
        }

        public void addNote(String note) {
            notes = note;
        }

        public float getPounds() {
            return itemSold.getWeightOf(quantitySold);
        }

        public long timeLong() {
            return timeOf.getTime();
        }

        public int getOrderId() {
            return orderId;
        }

        public String getFormattedOrderId() {
            return String.format(Locale.US, "%09d", orderId);
        }

        // col 0
        public String time() {
            return new SimpleDateFormat("HH:mm:ss MMM-dd").format(timeOf);
        }

        public String getDesc() {
            return time() + " | " + soldBy + " | " + paymentMethod + " | " + itemSold.name + " | " + quantitySold + " | " + unit().getUnitIdentifierSH(quantitySold) + " | "
                    + pricePerUnit() + " | " + total();
        }

        @Override
        public String toString() {
            return getDesc();
        }

        public String getSaveString() {
            return Integer.toString(getOrderId()) + DELIM + Long.toString(timeLong()) + DELIM + soldBy + DELIM + paymentMethod + DELIM + itemSold.name + DELIM + quantitySold + DELIM + notes;
        }

    }
}
