package com.sjarc.harvest.data;

public enum Unit {
    POUNDS("Pound", "lb"), OUNCES("Ounce", "oz"), PINTS("Pint", "pt"), QUARTS("Quart", "qt"),
    BUNCHES("Bunch"), HEADS("Head"), PLANTS("Plant"), ITEMS("Item");

    final String name;
    final String shorthand;
    final boolean doSH;

    Unit(String n) {
        name = n;
        shorthand = "";
        doSH = false;
    }

    Unit(String n, String sh) {
        name = n;
        shorthand = sh;
        doSH = true;
    }

    public String getUnitIdentifier() {
        return name + (name.endsWith("h") ? "es" : "s");
    }

    public String getUnitIdentifierSH() {
        return shorthand + "s";
    }

    public String getUnitIdentifier(double amount) {
        if (amount != 1D) return name + (name.endsWith("h") ? "es" : "s");
        else return name;
    }

    public String getUnitIdentifier(long amount) {
        if (amount != 1L) return name + (name.endsWith("h") ? "es" : "s");
        else return name;
    }

    public String getUnitIdentifierSH(double amount) {
        if (!doSH) return getUnitIdentifier(amount);
        if (amount != 1D) return shorthand + "s";
        else return shorthand;
    }

    public String getUnitIdentifierSH(long amount) {
        if (!doSH) return getUnitIdentifier(amount);
        if (amount != 1L) return shorthand + "s";
        else return shorthand;
    }

    @Override
    public String toString() {
        return name();
    }
}
