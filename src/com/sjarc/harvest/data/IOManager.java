package com.sjarc.harvest.data;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.widget.Toast;
import com.google.android.gms.common.api.*;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.*;
import com.sjarc.harvest.*;
import com.sjarc.harvest.data.History.Transaction;
import com.sjarc.harvest.data.ProduceItems.ProduceItem;
import java.io.*;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class IOManager {

    public static final String TRANSACTION_FILE_NAME = "transHist.dat";
    public static final String PRODUCE_FILE_NAME = "prodItems.dat";

    public static final char DELIM = ',';

    public static File transactionHistory;
    public static File produceItems;

    public static DriveFile cloudProduceItems;
    public static DriveFile cloudTransactionHistory;

    static Queue<Transaction> newTransactions = new LinkedList<>();



//    public static boolean checkAndDoFirstInit() {
//        File flag = new File(Harvest.currentInstance.context.getFilesDir(), "initFlag");
//        if (!flag.exists() || produceItems.length() <= 3) {
//            try {
//                //do first init
//
//                //create initial produceItems
//                for (int i = 0; i < PRODUCE_NAMES.length; i++) {
//                    ProduceItems.addProduce(PRODUCE_NAMES[i], PU[i], PRICE[i], PPU[i], false);
//                }
//
//                saveProduceItems();
//                flag.createNewFile();
//                return true;
//            } catch (IOException ex) {
//                Harvest.showError("Failed first init", ex);
//            }
//        }
//
//        return false;
//    }
    public static void initFiles() {
        transactionHistory = new File(Harvest.currentInstance.getFilesDir(), TRANSACTION_FILE_NAME);
        produceItems = new File(Harvest.currentInstance.getFilesDir(), PRODUCE_FILE_NAME);

        try {
            if (Harvest.currentInstance.getFilesDir().getFreeSpace() < 5_242_880L) {
                Toast.makeText(Harvest.currentInstance, "Not enough space - less than 5MB left", Toast.LENGTH_LONG).show();
                return;
            }
            if (!transactionHistory.exists()) {
                transactionHistory.createNewFile();
            }
            if (!produceItems.exists()) {
                produceItems.createNewFile();
            }

        } catch (IOException ex) {
            Harvest.showError("Unable to create save files");
        }
    }

    public static void initCloud(final Triggerable... triggers) {
        final GoogleApiClient api = Harvest.currentInstance.apiClient;

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    String historyS = Harvest.currentInstance.settings.getString("cloudTransactionHistory", "def");

                    if (historyS.equals("def")) {
                        //new file, so create it

                        changeCloudHistoryFile();

                    } else {
                        //read from drive id
                        DriveId id;
                        try {
                            id = DriveId.decodeFromString(historyS);
                        } catch (IllegalArgumentException ex) {
                            Harvest.currentInstance.settings.edit().putString("cloudTransactionHistory", "def").commit();
                            Harvest.showError("Cloud Transaction History file saved DriveId invalid (restart the app)!", ex);
                            return;
                        }
                        cloudTransactionHistory = id.asDriveFile();
                    }

                    String produceS = Harvest.currentInstance.settings.getString("cloudProduceItems", "def");

                    if (produceS.equals("def")) {
                        //new file, so create it

                        changeCloudProduceFile();

                    } else {
                        //read from drive id
                        DriveId id;
                        try {
                            id = DriveId.decodeFromString(produceS);
                        } catch (IllegalArgumentException ex) {
                            Harvest.currentInstance.settings.edit().putString("cloudProduceItems", "def").commit();
                            Harvest.showError("Cloud Produce Items file saved DriveId invalid (restart the app)!", ex);
                            return;
                        }
                        cloudProduceItems = id.asDriveFile();
                    }



                } catch (Throwable thrown) {
                    Harvest.showError("Something went wrong in cloud init: " + thrown.getMessage(), thrown);
                }
                Harvest.log("Login complete!");
                for (Triggerable t : triggers) {
                    t.trigger();
                }
            }
        }).start();
    }

    public static void registerNewTransaction(Transaction t) {
        newTransactions.add(t);
    }

    //convert to spreadsheet and push to Google Sheets
    public static void pushToCloud(final boolean pushProduce, final boolean pushHistory, final Triggerable... triggers) {
        //ensure all changes saved
        saveNewTransactions();
        Harvest.log("Started Cloud Push");

        //do Drive API stuff on separate thread
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    DriveContents cont;
                    Status s;

                    if (pushHistory) {

                        if (cloudTransactionHistory == null) {
                            Harvest.showError("Cloud Transaction History is null");
                            if (pushProduce) {
                                //push produce if need to
                                pushToCloud(true, false, triggers);
                            }
                            return;
                        }

                        //delete old file and open for edits
                        cont = cloudTransactionHistory.open(Harvest.currentInstance.apiClient, DriveFile.MODE_WRITE_ONLY, null).await().getDriveContents();

                        try (OutputStreamWriter out = new OutputStreamWriter(cont.getOutputStream())) {
                            out.write(getTransactionCSVString());
                            out.flush();
                        }

                        s = cont.commit(Harvest.currentInstance.apiClient, null).await().getStatus();
                        if (!s.isSuccess()) {
                            throw new Exception(s.getStatusMessage() + " - STATUS CODE: " + s.getStatusCode());
                        }
                    }

                    if (pushProduce) {

                        if (cloudProduceItems == null) {
                            Harvest.showError("Cloud Produce Items is null");
                            return;
                        }


                        //delete old file and open for edits
                        cont = cloudProduceItems.open(Harvest.currentInstance.apiClient, DriveFile.MODE_WRITE_ONLY, null).await().getDriveContents();

                        try (OutputStreamWriter out = new OutputStreamWriter(cont.getOutputStream())) {
                            out.write(getProduceItemCSVString());
                            out.flush();
                        }

                        s = cont.commit(Harvest.currentInstance.apiClient, null).await().getStatus();
                        if (!s.isSuccess()) {
                            throw new Exception(s.getStatusMessage() + " - STATUS CODE: " + s.getStatusCode());
                        }
                    }

                } catch (Throwable thrown) {
                    Harvest.showError("Something went wrong in pushing data to cloud: " + thrown.getMessage(), thrown);
                }
                Harvest.log("Finished Cloud Push");
                for (Triggerable t : triggers) {
                    t.trigger();
                }
            }
        }
        ).start();

    }

    public static void pullFromCloud(final boolean pullProduce, final boolean pullHistory, final Triggerable... triggers) {
        Harvest.log("Started Cloud Pull");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    DriveContents cont;
                    if (pullProduce) {

                        if (cloudProduceItems == null) {
                            Harvest.showError("Cloud Produce Items is null");
                            if (pullHistory) {
                                //finish pull if needed
                                pullFromCloud(false, true, triggers);
                            }
                            return;
                        }

                        cont = cloudProduceItems.open(Harvest.currentInstance.apiClient, DriveFile.MODE_READ_ONLY, null).await().getDriveContents();

                        try (Scanner in = new Scanner(cont.getInputStream())) {
                            if (in.hasNext()) {
                                in.useDelimiter("\\Z");
                                useProduceItemCSVString(in.next());
                            } else clearProduceItems();
                        }
                    }

                    if (pullHistory) {

                        if (cloudTransactionHistory == null) {
                            Harvest.showError("Cloud Transaction History is null");
                            return;
                        }

                        cont = cloudTransactionHistory.open(Harvest.currentInstance.apiClient, DriveFile.MODE_READ_ONLY, null).await().getDriveContents();

                        try (Scanner in = new Scanner(cont.getInputStream())) {
                            if (in.hasNext()) {
                                in.useDelimiter("\\Z");
                                useTransactionCSVString(in.next());
                            } else clearTransactionHistory();
                        }
                    }


                } catch (Throwable thrown) {
                    Harvest.showError("Something went wrong in pulling data from cloud: " + thrown.getMessage(), thrown);
                }
                Harvest.log("Finished Cloud Pull");
                for (Triggerable t : triggers) {
                    t.trigger();
                }
            }
        }).start();
    }

    public static void useTransactionCSVString(String csv) {
        clearTransactionHistory();

        //Harvest.log(csv);

        String[] trans = csv.replace('\"', ' ').split("\n");
        for (int i = trans.length - 1; i > 0; i--) {
            String[] t = trans[i].split(Character.toString(DELIM));
            //queue transactions for saving
            try {
                if (t.length == 1) throw new Exception("Line is empty");
                if (t.length != 11) throw new Exception("Line does not contain needed information, or format is incorrect");
                History.addTransaction(new Transaction(ProduceItems.getProduce(t[4].trim()), Float.parseFloat(t[5].trim()), t[2].trim(), t[10].trim(), t[3].trim(), new SimpleDateFormat("HH:mm:ss MMM-dd").parse(t[1].trim()), Integer.parseInt(t[0].trim())), true);
            } catch (Throwable thrown) {
                Harvest.showError("Unable to process row " + (i + 1) + " of Transaction History!", thrown);
            }
        }
        saveNewTransactions();
    }

    public static void useProduceItemCSVString(String csv) {
        clearProduceItems();

        //Harvest.log(csv);

        String[] trans = csv.replace('\"', ' ').split("\n");

        for (int i = 1; i < trans.length; i++) {
            String[] t = trans[i].split(Character.toString(DELIM));



            //queue produce for saving
            try {
                //make sure stuff is ready and no empty lines are read
                if (t.length == 1) throw new Exception("Line is empty");
                if (t.length != 4) throw new Exception("Line does not contain needed information, or format is incorrect");
                ProduceItems.addProduce(t[0].trim(), Unit.valueOf(t[1].trim()), Math.round(NumberFormat.getCurrencyInstance(Locale.US).parse(t[2].trim()).floatValue() * 100f), Float.parseFloat(t[3].trim()), false);
            } catch (Throwable thrown) {
                Harvest.showError("Unable to process line " + i + " of Produce Items!", thrown);
            }
        }
        saveProduceItems();
    }

    public static String getTransactionCSVString() {
        saveNewTransactions();
        StringBuilder builder = new StringBuilder();

        //label lines
        builder.append("\"").append(Harvest.currentInstance.getString(R.string.col_1_label)).append("\"").append(DELIM);
        builder.append("\"").append(Harvest.currentInstance.getString(R.string.col_2_label)).append("\"").append(DELIM);
        builder.append("\"").append(Harvest.currentInstance.getString(R.string.col_3_label)).append("\"").append(DELIM);
        builder.append("\"").append(Harvest.currentInstance.getString(R.string.col_4_label)).append("\"").append(DELIM);
        builder.append("\"").append(Harvest.currentInstance.getString(R.string.col_5_label)).append("\"").append(DELIM);
        builder.append("\"").append(Harvest.currentInstance.getString(R.string.col_6_label)).append("\"").append(DELIM);
        builder.append("\"").append(Harvest.currentInstance.getString(R.string.col_7_label)).append("\"").append(DELIM);
        builder.append("\"").append(Harvest.currentInstance.getString(R.string.col_8_label)).append("\"").append(DELIM);
        builder.append("\"").append(Harvest.currentInstance.getString(R.string.col_9_label)).append("\"").append(DELIM);
        builder.append("\"").append(Harvest.currentInstance.getString(R.string.col_10_label)).append("\"").append(DELIM);
        builder.append("\"").append("Notes").append("\"").append("\n");


        for (Transaction t : History.getTransactions()) {
            builder.append("\"").append(t.getFormattedOrderId()).append("\"").append(DELIM);
            builder.append("\"").append(t.time()).append("\"").append(DELIM);
            builder.append("\"").append(t.soldBy).append("\"").append(DELIM);
            builder.append("\"").append(t.paymentMethod).append("\"").append(DELIM);
            builder.append("\"").append(t.itemSold.name).append("\"").append(DELIM);
            builder.append("\"").append(t.quantitySold).append("\"").append(DELIM);
            builder.append("\"").append(t.unit().getUnitIdentifierSH(t.quantitySold)).append("\"").append(DELIM);
            builder.append("\"").append(t.getPounds()).append("\"").append(DELIM);
            builder.append("\"").append(NumberFormat.getCurrencyInstance(Locale.US).format(t.pricePerUnit())).append("\"").append(DELIM);
            builder.append("\"").append(NumberFormat.getCurrencyInstance(Locale.US).format(t.total())).append("\"").append(DELIM);
            builder.append("\"").append(t.note()).append("\"").append("\n");
        }


        return builder.toString();
    }

    public static String getProduceItemCSVString() {
        saveNewTransactions();
        StringBuilder builder = new StringBuilder();

        //label lines
        builder.append("\"").append("Name").append("\"").append(DELIM);
        builder.append("\"").append("Unit").append("\"").append(DELIM);
        builder.append("\"").append("Price per Unit").append("\"").append(DELIM);
        builder.append("\"").append("Pounds per Unit").append("\"").append("\n");


        for (ProduceItem p : ProduceItems.getProduce()) {
            builder.append("\"").append(p.name).append("\"").append(DELIM);
            builder.append("\"").append(p.defaultUnit).append("\"").append(DELIM);
            builder.append("\"").append(NumberFormat.getCurrencyInstance(Locale.US).format(p.getPriceOf(1) / 100f)).append("\"").append(DELIM);
            builder.append("\"").append(p.poundsPerUnit).append("\"").append("\n");
        }

        return builder.toString();
    }

    public static void changeCloudProduceFile() {
        Harvest.currentInstance.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {

                    //chose between open and create
                    AlertDialog.Builder builder = new AlertDialog.Builder(Harvest.currentInstance);
                    builder.setCancelable(false);
                    builder.setTitle("Set Cloud Produce Items File");
                    builder.setMessage("Do you wish to open an existing file, or create a new file? \nIf you create a new file, any existing transactions in the current transaction history file may not save - if you are starting a new market, change the transaction history file first!");
                    builder.setPositiveButton(
                            "OPEN",
                            new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //OPEN code
                            IntentSender intentSender = Drive.DriveApi.newOpenFileActivityBuilder()
                                    .setMimeType(new String[]{"text/csv"})
                                    .setActivityTitle("Choose a produce items file to open")
                                    .build(Harvest.currentInstance.apiClient);


                            try {
                                Harvest.currentInstance.startIntentSenderForResult(intentSender, Harvest.RESULT_CLOUD_PRODUCE, null, 0, 0, 0);
                            } catch (SendIntentException ex) {
                                Harvest.showError("Error in changing produce item file - could not open external file choosing intent", ex);
                            }
                        }
                    });
                    builder.setNegativeButton(
                            "CREATE",
                            new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which
                        ) {
                            //CREATE code
                            Drive.DriveApi.newDriveContents(Harvest.currentInstance.apiClient).setResultCallback(new ResultCallback<DriveContentsResult>() {
                                @Override
                                public void onResult(DriveContentsResult cont) {
                                    IntentSender intentSender = Drive.DriveApi.newCreateFileActivityBuilder()
                                            .setActivityTitle("Create a new produce items file")
                                            .setInitialDriveContents(cont.getDriveContents())
                                            .setInitialMetadata(new MetadataChangeSet.Builder().setMimeType("text/csv").setDescription("A Produce Items file for the Harvest app").build())
                                            .build(Harvest.currentInstance.apiClient);

                                    try {
                                        Harvest.currentInstance.startIntentSenderForResult(intentSender, Harvest.RESULT_CLOUD_PRODUCE, null, 0, 0, 0);
                                    } catch (SendIntentException ex) {
                                        Harvest.showError("Error in changing produce item file - could not open external file choosing intent", ex);
                                    }
                                }

                            });
                        }
                    });
                    builder.show();
                } catch (Throwable ex) {
                    Harvest.showError("Error in changing produce item file", ex);
                }
            }
        }
        );
    }

    public static void setCloudProduceFile(DriveId id) {
        cloudProduceItems = id.asDriveFile();
        Harvest.currentInstance.settings.edit().putString("cloudProduceItems", id.encodeToString()).commit();
        //update produce list
        pullFromCloud(true, false, new Triggerable() {
                  @Override
                  public void trigger() {
                      Harvest.currentInstance.managementFragment.refresh();
                  }
              });
    }

    //very similar code to produce file change method. Perhaps later can optimize code-wise.
    public static void changeCloudHistoryFile() {
        Harvest.currentInstance.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {

                    //chose between open and create
                    AlertDialog.Builder builder = new AlertDialog.Builder(Harvest.currentInstance);
                    builder.setCancelable(false);
                    builder.setTitle("Set Cloud Transaction History File");
                    builder.setMessage("Do you wish to open an existing file, or create a new file?");
                    builder.setPositiveButton(
                            "OPEN",
                            new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //OPEN code
                            IntentSender intentSender = Drive.DriveApi.newOpenFileActivityBuilder()
                                    .setMimeType(new String[]{"text/csv"})
                                    .setActivityTitle("Choose a transaction history file to open")
                                    .build(Harvest.currentInstance.apiClient);


                            try {
                                Harvest.currentInstance.startIntentSenderForResult(intentSender, Harvest.RESULT_CLOUD_HISTORY, null, 0, 0, 0);
                            } catch (SendIntentException ex) {
                                Harvest.showError("Error in changing transaction history file - could not open external file choosing intent", ex);
                            }
                        }
                    });

                    builder.setNegativeButton(
                            "CREATE",
                            new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //CREATE code
                            Drive.DriveApi.newDriveContents(Harvest.currentInstance.apiClient).setResultCallback(new ResultCallback<DriveContentsResult>() {
                                @Override
                                public void onResult(DriveContentsResult cont) {
                                    IntentSender intentSender = Drive.DriveApi.newCreateFileActivityBuilder()
                                            .setActivityTitle("Create a new transaction history file")
                                            .setInitialDriveContents(cont.getDriveContents())
                                            .setInitialMetadata(new MetadataChangeSet.Builder().setMimeType("text/csv").setDescription("A Transaction History file for the Harvest app").build())
                                            .build(Harvest.currentInstance.apiClient);

                                    try {
                                        Harvest.currentInstance.startIntentSenderForResult(intentSender, Harvest.RESULT_CLOUD_HISTORY, null, 0, 0, 0);
                                    } catch (SendIntentException ex) {
                                        Harvest.showError("Error in changing transaction history file - could not open external file choosing intent", ex);
                                    }
                                }
                            });

                        }
                    });
                    builder.show();
                } catch (Throwable ex) {
                    Harvest.showError("Error in changing transaction history file", ex);
                }
            }
        });
    }

    public static void setCloudHistoryFile(DriveId id) {
        cloudTransactionHistory = id.asDriveFile();
        Harvest.currentInstance.settings.edit().putString("cloudTransactionHistory", id.encodeToString()).commit();
        //update transaction history
        pullFromCloud(false, true, new Triggerable() {
                  @Override
                  public void trigger() {
                      Harvest.currentInstance.managementFragment.refresh();
                  }
              });
    }

    public static void clearProduceItems() {
        produceItems.delete();
        initFiles();
        ProduceItems.init();
    }

    public static void clearTransactionHistory() {

        transactionHistory.delete();
        newTransactions.clear(); //clear any pending transactions as well - ones made this session and not saved
        initFiles();
        History.init();
    }

    public static void loadProduceItems() {

        try (Scanner in = new Scanner(produceItems)) {

            while (in.hasNextLine()) {
                //add produce
                ProduceItems.ProduceItem it = ProduceItems.addProduce(in.nextLine(), false);
            }
            //should not need this, anything added not from here will be saved if done correctly
            //saveProduceItems();
        } catch (FileNotFoundException ex) {
            Harvest.showError("Unable to read ProduceItem list", ex);
        }

    }

    //since produce item modification is possible, must always write out entire list (or only modified or added ones, but that is harder than just added ones like transactions are written)
    public static void saveProduceItems() {
        //delete produce file and re-create it empty
        produceItems.delete();
        initFiles();

        FileWriter out = null;
        try {
            out = new FileWriter(produceItems, true);

            for (ProduceItems.ProduceItem it : ProduceItems.getProduce()) {
                out.append(it.getSaveString() + "\n");
            }

            out.flush();

        } catch (Exception ex) {
            Harvest.showError("Unable to write produce items");
        } finally {

            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                Harvest.showError("Can't close FileWriter", ex);
            }
        }

    }

    public static void loadPastTransactions() {

        try (Scanner in = new Scanner(transactionHistory)) {

            while (in.hasNextLine()) {
                History.addTransaction(new History.Transaction(in.nextLine()), false);
            }

        } catch (Exception ex) {
            Harvest.showError("Unable to load transaction history", ex);
        }
    }

    //just write out all transactions after the last date in the file
    public static void saveNewTransactions() {
        if (newTransactions.isEmpty()) return;

        try (FileWriter out = new FileWriter(transactionHistory, true)) {

            while (!newTransactions.isEmpty()) {
                out.append(newTransactions.remove().getSaveString() + "\n");
            }

            out.flush();

        } catch (Exception ex) {
            Harvest.showError("Unable to write transaction history");
        }

    }

    public static void debug(File f, boolean newLines) {
        StringBuilder b = new StringBuilder();
        try (Scanner in = new Scanner(f)) {

            while (in.hasNextLine()) {
                b.append(in.nextLine());
                if (newLines) b.append("\n");
                else b.append("; ");
            }

            Harvest.log(f.getName() + ": " + b.toString());

        } catch (Exception ex) {
            Harvest.showError("Unable to read file", ex);
        }
    }
}
